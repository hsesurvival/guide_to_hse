﻿using HseSurvival.Core;
using HseSurvival.Core.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace HseSurvival.UI
{
    /// <summary>
    /// Interaction logic for RegistrationWindow.xaml
    /// </summary>
    public partial class RegistrationWindow : Window
    {
        public RegistrationWindow()
        {
            InitializeComponent();
        }

        private void ButtonJoin_Click(object sender, RoutedEventArgs e)
        {
            if (EmailValidation.isEmailHse(Email_box.Text))
            {

                if (DatabaseRepository.IsRegistred(Email_box.Text) == "not")
                {
                    DatabaseRepository.ConfirmEmail(Email_box.Text, Name_box.Text, Group_box.Text);
                    var confirmWindow = new ConfirmWindow(Email_box.Text);
                    confirmWindow.Show();
                    this.Close();
                }
                else MessageBox.Show("Уже есть пользователь с такой почтой", "Error");
            }
            else MessageBox.Show("Нам нужна твоя корпоративная почта! Проверь правильность введенных данных", "Error");
        }
    }
}
