﻿using HseSurvival.Core.Helpers;
using HseSurvival.Core.Models;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace HseSurvival.UI
{
    /// <summary>
    /// Interaction logic for SubjectsPage.xaml
    /// </summary>
    public partial class SubjectsPage : Page
    {
        DatabaseRepository repo = new DatabaseRepository();
        public SubjectsPage()
        {
            InitializeComponent();
            var disciplines = Caching.Get<List<Discipline>>("Diciplines");

            if (disciplines == null)
            {
                disciplines = repo.LoadData("Бизнес-информатика");
                Caching.Add("Diciplines", disciplines);
            }
            ListOfDisciplines.ItemsSource = disciplines;

        }
        // копипаста https://stackoverflow.com/questions/16234522/scrollviewer-mouse-wheel-not-working
        private void ListViewScrollViewer_PreviewMouseWheel(object sender, System.Windows.Input.MouseWheelEventArgs e)
        {
            ScrollViewer scv = (ScrollViewer)sender;
            scv.ScrollToVerticalOffset(scv.VerticalOffset - e.Delta);
            e.Handled = true;
        }
        //
        private void Hyperlink_RequestNavigate(object sender, RequestNavigateEventArgs e)
        {
            Process.Start(new ProcessStartInfo(e.Uri.AbsoluteUri));
            e.Handled = true;
        }
        

        private void FormulaResponse(object sender, RoutedEventArgs e)
        {
            var selectedDiscipline = ListOfDisciplines.SelectedItem as Discipline;
            var messageWindow = new MessageWindow(selectedDiscipline);
            messageWindow.Show();
        }
    }
}
