﻿using HseSurvival.Core;
using HseSurvival.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace HseSurvival.UI
{
    /// <summary>
    /// Interaction logic for GradesWindow.xaml
    /// </summary>
    public partial class GradesWindow : Window
    {
        bool StateClosed = true;

        public GradesWindow()
        {
            InitializeComponent();

            ForPages.Content = new GradesPage();
        }

        private void ButtonMenu_Click(object sender, RoutedEventArgs e)
        {
            if (StateClosed)
            {
                Storyboard sb = this.FindResource("OpenMenu") as Storyboard;
                sb.Begin();
                ButtonGrades.Visibility = Visibility.Visible;
                ButtonCalculator.Visibility = Visibility.Visible;
                ButtonSubjects.Visibility = Visibility.Visible;
                ButtonRules.Visibility = Visibility.Visible;
                ButtonFeedback.Visibility = Visibility.Visible;
                ButtonProfile.Visibility = Visibility.Visible;
                textblock_note.Visibility = Visibility.Visible;
            }
            else
            {
                Storyboard sb = this.FindResource("CloseMenu") as Storyboard;
                sb.Begin();
                ButtonGrades.Visibility = Visibility.Hidden;
                ButtonCalculator.Visibility = Visibility.Hidden;
                ButtonSubjects.Visibility = Visibility.Hidden;
                ButtonRules.Visibility = Visibility.Hidden;
                ButtonFeedback.Visibility = Visibility.Hidden;
                ButtonProfile.Visibility = Visibility.Hidden;
                textblock_note.Visibility = Visibility.Hidden;
            }

            StateClosed = !StateClosed;
        }

        private void ButtonCalculator_Click(object sender, RoutedEventArgs e)
        {
            ForPages.Content = null;
            ForPages.Content = new CalculatorPage(ForPages);
        }

        private void ButtonSubjects_Click(object sender, RoutedEventArgs e)
        {
            ForPages.Content = null;
            ForPages.Content = new SubjectsPage();
        }

        private void ButtonGrades_Click(object sender, RoutedEventArgs e)
        {
            ForPages.Content = null;
            ForPages.Content = new GradesPage();
        }

        private void ButtonRules_Click(object sender, RoutedEventArgs e)
        {
            ForPages.Content = null;
            ForPages.Content = new RulesPage();
        }

        private void ButtonFeedback_Click(object sender, RoutedEventArgs e)
        {
            ForPages.Content = null;
            ForPages.Content = new FeedbackPage();
        }

        private void ButtonProfile_Click(object sender, RoutedEventArgs e)
        {
            ForPages.Content = null;
            ForPages.Content = new ProfilePage();
        }
    }
}
