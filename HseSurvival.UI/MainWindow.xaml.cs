﻿using HseSurvival.Core.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace HseSurvival.UI
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        int k = 0;
        public MainWindow()
        {
            InitializeComponent();

        }

        private void ShowPassword()
        {
            Thickness margin = PasswordBoxVisible.Margin;
            margin.Left = -10;
            pass_box.Margin = margin;
            PasswordBoxVisible.Visibility = Visibility.Visible;
            pass_box.Visibility = Visibility.Hidden;
            pass_box.Width = 0;
            PasswordBoxVisible.Width = 270;
            PasswordBoxVisible.Height = 40;
            PasswordBoxVisible.Background = Brushes.White;
            PasswordBoxVisible.Text = pass_box.Password;
        }

        private void HidePassword()
        {
            PasswordBoxVisible.Text = null;
            PasswordBoxVisible.Visibility = Visibility.Hidden;
            PasswordBoxVisible.Width = 0;
            pass_box.Width = 270;
            Thickness margin = PasswordBoxVisible.Margin;
            margin.Left = 0;
            pass_box.Margin = margin;
            pass_box.Visibility = Visibility.Visible;
            pass_box.Password = PasswordBoxVisible.Text;
            pass_box.Focus();
        }

        private void ButtonSignIn_Click(object sender, RoutedEventArgs e)
        {
            if ((string.IsNullOrWhiteSpace(textBox_login.Text) || (string.IsNullOrWhiteSpace(pass_box.Password)))) MessageBox.Show("Не оставляй поля пустыми!");
            else
            if (pass_box.Password.Length < 6) { MessageBox.Show("У тебя не может быть такой короткий пароль...", "Eror"); }
            else {
                string db_result = DatabaseRepository.IsRegistred(textBox_login.Text);
                if (db_result != "not")
                {
                    if (DatabaseRepository.GetHash(pass_box.Password) == db_result)
                    {
                        var gradesWindow = new GradesWindow();
                        gradesWindow.Show();
                        this.Close();
                    }
                    else MessageBox.Show("Проверь пароль!");
            }
                
            }
        }

        private void eye_button_Click(object sender, RoutedEventArgs e)
        {
            k++;
            if (k % 2 ==0)
            {
                HidePassword();
            }
            else
            {
                ShowPassword();
            }
        }

        private void ButtonForgetPass_Click(object sender, RoutedEventArgs e)
        {
            var forgetPassWindow = new ForgetPassword();
            forgetPassWindow.Show();
            this.Close();
        }

        private void ButtonRegistration_Click(object sender, RoutedEventArgs e)
        {
            var registrationWindow = new RegistrationWindow();
            registrationWindow.Show();
            this.Close();
        }

        private void Window_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                ButtonSignIn_Click(sender, e);
                e.Handled = true;
            }
        }        
    }
}
