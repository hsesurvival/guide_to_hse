﻿using HseSurvival.Core;
using HseSurvival.Core.Helpers;
using HseSurvival.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace HseSurvival.UI
{
    /// <summary>
    /// Interaction logic for GradesPage.xaml
    /// </summary>
    public partial class GradesPage : Page
    {
        DatabaseRepository repo = new DatabaseRepository();
        CalculatorHelpers help = new CalculatorHelpers();

        public delegate string MakeItBeDouble(string text);
        MakeItBeDouble delegator;

        Discipline discipline;
        public GradesPage()
        {
            InitializeComponent();
            var disciplines = Caching.Get<List<Discipline>>("DiciplinesForCalc");

            if (disciplines == null)
            {
                disciplines = repo.LoadDataForCalc("Бизнес-информатика");
                Caching.Add("DiciplinesForCalc", disciplines);
            }
            combobox_disciplines.ItemsSource = disciplines;
        }

        private void combobox_disciplines_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            MainStackpanel.Children.Clear();
            textbox_exam?.Clear();
            textbox_exam.BorderBrush = Brushes.LightGray;
            textbox_result?.Clear();
            textblock_warning.Visibility = Visibility.Hidden;
            var selectedSubject = combobox_disciplines.SelectedItem as Discipline;
            discipline = repo.LoadDataForCalc("Бизнес-информатика").FirstOrDefault(d => d.Name == selectedSubject.Name);

            for (int i = 0; i < discipline.FormsOfControl.Count; i++)
            {
                var stackpanel =
                    new StackPanel
                    {
                        Orientation = Orientation.Vertical,
                        VerticalAlignment = VerticalAlignment.Center,
                        HorizontalAlignment = HorizontalAlignment.Center,
                        Margin = new Thickness(10)
                    };
                stackpanel.Children.Add(new TextBlock
                {
                    Text = discipline.FormsOfControl[i].Name,
                    Margin = new Thickness(0, 0, 0, 10),
                    Foreground = Brushes.CornflowerBlue,
                    FontSize = 30,
                    FontFamily = new FontFamily("Yu Gothic UI Semibold"),
                    TextAlignment = TextAlignment.Center
                });
                stackpanel.Children.Add(new TextBox
                {
                    BorderBrush = Brushes.LightGray,
                    Width = 60,
                    Height = 60,
                    FontSize = 30,
                    FontFamily = new FontFamily("Yu Gothic UI Semibold"),
                    TextAlignment = TextAlignment.Center,
                    VerticalContentAlignment = VerticalAlignment.Center
                });

                MainStackpanel.Children.Add(stackpanel);

                stackpanel_exam.Visibility = Visibility.Visible;
                stackpanel_totalResult.Visibility = Visibility.Visible;
                ButtonCount.Visibility = Visibility.Visible;
            }
        }

        public void UpdateGrades()
        {
            textboxesWithoutValue?.Clear();
            textbox_exam?.Clear();
            textbox_exam.BorderBrush = Brushes.LightGray;
            textbox_result?.Clear();
            textblock_warning.Visibility = Visibility.Hidden;
            for (int i = 0; i < MainStackpanel.Children.Count; i++)
            {
                var stackpanel = MainStackpanel.Children[i] as StackPanel;
                var textboxWithGrade = stackpanel.Children[1] as TextBox;

                textboxWithGrade?.Clear();
                textboxWithGrade.BorderBrush = Brushes.LightGray;
            }
        }


        List<TextBox> textboxesWithoutValue;
        private void ButtonCount_Click(object sender, RoutedEventArgs e)
        {
            delegator = help.ReplaceDotToComma;
            textboxesWithoutValue = new List<TextBox>();
            double totalWeightWithoutGrade = 0;

            double partsOfAggrGrade = 0; //оценки частей накопа, умноженных на их вес в накопе

            try
            {
                for (int i = 0; i < MainStackpanel.Children.Count; i++)
                {
                    var stackpanel = MainStackpanel.Children[i] as StackPanel;
                    var textblockWithName = stackpanel.Children[0] as TextBlock;
                    var textboxWithGrade = stackpanel.Children[1] as TextBox;

                    var formOfControl = discipline.FormsOfControl.FirstOrDefault(f => f.Name == textblockWithName.Text);

                    double partOfAggreGrade; //введенные оценки в частях накопа

                    if (!String.IsNullOrEmpty(textboxWithGrade.Text))
                    {
                        partOfAggreGrade = double.Parse(textboxWithGrade.Text);

                        if (partOfAggreGrade > 10)
                        {
                            partOfAggreGrade = double.Parse(delegator(textboxWithGrade.Text));
                        }
                        partsOfAggrGrade = help.CalculateAccumGrade(formOfControl, partOfAggreGrade, partsOfAggrGrade);
                    }
                    else if (String.IsNullOrEmpty(textboxWithGrade.Text) && !String.IsNullOrEmpty(textbox_result.Text))
                    {
                        textboxWithGrade.BorderBrush = Brushes.Red;
                        totalWeightWithoutGrade += (formOfControl.Weight / 100);
                        textboxesWithoutValue.Add(textboxWithGrade);
                    }
                    else if (String.IsNullOrEmpty(textboxWithGrade.Text) && String.IsNullOrEmpty(textbox_result.Text))
                    {
                        MessageBox.Show("Мы должны знать, какую итоговую оценку ты хочешь, чтобы рассчитать, " +
                            "что ты должен получить за ту или иную работу! Прочитай, пожалуйста, Правила!", "Внимание!", MessageBoxButton.OKCancel);
                        textbox_result.Text = "-";
                    }
                }

                double aggrGrade = partsOfAggrGrade * (discipline.AccumWeight / 100);

                double partOfExamGrade;
                double examGrade = 0;

                if (!String.IsNullOrEmpty(textbox_exam.Text)) //заполненная
                {
                    partOfExamGrade = double.Parse(textbox_exam.Text);

                    if (partOfExamGrade > 10)
                    {
                        partOfExamGrade = double.Parse(delegator(textbox_exam.Text));
                    }
                    examGrade = partOfExamGrade * (discipline.ExamWeight / 100);
                }
                else if (String.IsNullOrEmpty(textbox_exam.Text) && !String.IsNullOrEmpty(textbox_result.Text)) //не заполненная
                {
                    textbox_exam.BorderBrush = Brushes.Red;
                }
                else if (String.IsNullOrEmpty(textbox_exam.Text) && String.IsNullOrEmpty(textbox_result.Text))
                {
                    MessageBox.Show("Мы должны знать, какую итоговую оценку ты хочешь, чтобы рассчитать, " +
                       "что ты должен получить за ту или иную работу! Прочитай, пожалуйста, Правила!", "Внимание!", MessageBoxButton.OKCancel);
                    textbox_result.Text = "-";
                }

                double totalResult;

                if (!String.IsNullOrEmpty(textbox_result.Text))
                {
                    totalResult = double.Parse(textbox_result.Text);
                    if (totalResult > 10)
                    {
                        totalResult = double.Parse(delegator(textbox_result.Text));//СДЕЛАТЬ ДЕЛЕГАТ
                    }

                    if (!String.IsNullOrEmpty(textbox_exam.Text) && textboxesWithoutValue.Count != 0) //только пустой накоп
                    {
                        var result = help.CalculateResultWithEmptyPartOfAggrGrade(totalResult, examGrade, aggrGrade, totalWeightWithoutGrade);
                        foreach (var textbox in textboxesWithoutValue)
                        {
                            textbox.Text = result.ToString();
                        }
                    }
                    else if (String.IsNullOrEmpty(textbox_exam.Text) && textboxesWithoutValue == null) //только пустой экзамен
                    {
                        var result = (totalResult - aggrGrade) / (discipline.ExamWeight / 100);
                    }
                    else if (textboxesWithoutValue != null && String.IsNullOrEmpty(textbox_exam.Text)) //пустые и экзамен и что-то из накопа
                    {
                        var result = help.CalculateResultWithEmptyExamAndAggrGrades(totalResult, aggrGrade, discipline.ExamWeight, discipline.AccumWeight, totalWeightWithoutGrade);
                        foreach (var textbox in textboxesWithoutValue)
                        {
                            textbox.Text = result.ToString();
                        }
                        textbox_exam.Text = result.ToString();
                    }
                }
                else
                {
                    textbox_result.Text = (aggrGrade + examGrade).ToString();
                }

                if (!String.IsNullOrEmpty(textbox_result.Text) && double.Parse(textbox_result.Text) < 4)
                {
                    textblock_warning.Visibility = Visibility.Visible;
                }
            }
            catch
            {
                CustomException c = new CustomException("Оценка - это число. Проверь пожалуйста введенные данные.");
                MessageBox.Show(c.Message, "Внимание!", MessageBoxButton.OKCancel);
                UpdateGrades();
            }
        }

        private void ButtonRefresh_Click(object sender, RoutedEventArgs e)
        {
            UpdateGrades();
        }

        private void Page_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                ButtonCount_Click(sender, e);
                e.Handled = true;
            }
        }
    }
}

