﻿using HseSurvival.Core.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace HseSurvival.UI
{
    /// <summary>
    /// Логика взаимодействия для ChangePassPage.xaml
    /// </summary>
    public partial class ChangePassPage : Page
    {
        string email;
        Window confirmWindow;
        public ChangePassPage(string user_email, Window _confirmWindow)
        {
            InitializeComponent();
            email = user_email;
            confirmWindow = _confirmWindow;
        }

        private void Confirm_Click(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrWhiteSpace(password_textbox.Text) || string.IsNullOrWhiteSpace(password_confirm_textbox.Text)) MessageBox.Show("Не оставляй поля пустыми!");
            else
            { if ((password_textbox.Text.Length < 6) || (password_confirm_textbox.Text.Length < 6)) MessageBox.Show("Слишком короткий пароль!");
            else 
                if (password_textbox.Text == password_confirm_textbox.Text)
                {
                    DatabaseRepository.ResetPassword(password_confirm_textbox.Text, email);
                    MessageBox.Show("Пароль был успешно сменен!");

                    var signInWindow = new MainWindow();
                    signInWindow.Show();
                    confirmWindow.Close();
                }
                else MessageBox.Show("Введи совпадающие пароли!", "Ошибка");
            }
        }
    }
}
