﻿using HseSurvival.Core.Helpers;
using HseSurvival.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace HseSurvival.UI
{
    /// <summary>
    /// Interaction logic for CalculatorPage.xaml
    /// </summary>
    public partial class CalculatorPage : Page
    {
        private CalculatorHelpers help = new CalculatorHelpers();

        public delegate string MakeItBeDouble(string text);
        MakeItBeDouble delegator;

        public List<FormOfControl> formsOfControl = new List<FormOfControl>();
        private Frame frame;
        public CalculatorPage(Frame _frame)
        {
            InitializeComponent();
            frame = _frame;
        }

        private void ButtonAdd_Click(object sender, RoutedEventArgs e)
        {
            var stackpanel = new StackPanel
            {
                Orientation = Orientation.Horizontal
            };

            stackpanel.Children.Add(
                new TextBox
                {
                    Width = 200,
                    Height = 40,
                    BorderBrush = Brushes.LightGray,
                    VerticalContentAlignment = VerticalAlignment.Center,
                    TextAlignment = TextAlignment.Center,
                    FontFamily = new FontFamily("Yu Gothic UI Semibold"),
                    FontSize = 18
                }
            );
            stackpanel.Children.Add(
               new TextBox
               {
                   Width = 150,
                   Height = 40,
                   BorderBrush = Brushes.LightGray,
                   VerticalContentAlignment = VerticalAlignment.Center,
                   TextAlignment = TextAlignment.Center,
                   FontFamily = new FontFamily("Yu Gothic UI Semibold"),
                   FontSize = 18
               }
            );
            stackpanel_weight.Children.Add(stackpanel);

            if (stackpanel_weight.Children.Count >= 7)
            {
                scrollbar.VerticalScrollBarVisibility = ScrollBarVisibility.Visible;
            }
        }

        private void ButtonNext_Click(object sender, RoutedEventArgs e)
        {
            delegator = help.ReplaceDotToComma;
            //List<string> failToEnterWeight = new List<string>();
            List<string> notFilledWeight = new List<string>();
            List<string> notFilledName = new List<string>();
            double totalWeight = 0;

            try
            {
                //transform written information (form of control and its weight into an object of FormOfControl class)
                for (int i = 1; i < stackpanel_weight.Children.Count; i++)
                {
                    var formOfControl = stackpanel_weight.Children[i] as StackPanel;
                    var textboxWithWork = formOfControl.Children[0] as TextBox;
                    var nameOfWork = textboxWithWork.Text;
                    var textboxWeight = formOfControl.Children[1] as TextBox;

                    double weightOfWork = 0;
                    if (!String.IsNullOrEmpty(textboxWeight.Text)) //everything is ok
                    {
                        weightOfWork = double.Parse(textboxWeight.Text);

                        if (weightOfWork <= 100)
                        {
                            formsOfControl.Add(
                            new FormOfControl
                            {
                                Name = nameOfWork,
                                Weight = weightOfWork
                            });
                        }
                        else
                        {
                            weightOfWork = double.Parse(delegator(textboxWeight.Text));
                            formsOfControl.Add(
                            new FormOfControl
                            {
                                Name = nameOfWork,
                                Weight = weightOfWork
                            });
                        }
                    }
                    else if (!String.IsNullOrEmpty(nameOfWork) && String.IsNullOrEmpty(textboxWeight.Text)) //empty weight value
                    {

                        notFilledWeight.Add(textboxWeight.Text);
                    }
                    else if (String.IsNullOrEmpty(nameOfWork) && !String.IsNullOrEmpty(textboxWeight.Text)) //empty name of work
                    {

                        notFilledName.Add(nameOfWork);
                    }
                    totalWeight += weightOfWork;
                }

                double aggrGradeWeight = double.Parse(textbox_aggrGradeWeight.Text);
                double examWeight = double.Parse(textbox_examWeight.Text);

                if (aggrGradeWeight > 100)
                {
                    aggrGradeWeight = double.Parse(delegator(textbox_aggrGradeWeight.Text));
                }
                if (examWeight > 100)
                {
                    examWeight = double.Parse(delegator(textbox_examWeight.Text));
                }

                if (String.IsNullOrEmpty(textbox_aggrGradeWeight.Text) || String.IsNullOrEmpty(textbox_examWeight.Text))
                {
                    MessageBox.Show("Заполни веса экзамена и накопа! Прочитай, пожалуйста, Правила.", "Внимание!", MessageBoxButton.OKCancel);
                    formsOfControl.Clear();
                }
                else if (notFilledWeight.Count != 0)
                {
                    MessageBox.Show("У тебя не заполнен вес работы! Прочитай, пожалуйста, Правила.", "Внимание!", MessageBoxButton.OKCancel);
                    formsOfControl.Clear();
                }
                else if (notFilledName.Count != 0)
                {
                    MessageBox.Show("У тебя не заполнено название работы! Прочитай, пожалуйста, Правила.", "Внимание!", MessageBoxButton.OKCancel);
                    formsOfControl.Clear();
                }
                else if ((totalWeight != 100 && totalWeight != 0) || (aggrGradeWeight + examWeight) != 100)
                {
                    MessageBox.Show("Сумма весов не равна 100%! Прочитай, пожалуйста, Правила.", "Внимание!", MessageBoxButton.OKCancel);
                    formsOfControl.Clear();
                }
                else
                {
                    frame.Content = new ActualCalculator(frame, formsOfControl, double.Parse(textbox_examWeight.Text), double.Parse(textbox_aggrGradeWeight.Text));
                }
            }
            catch
            {
                CustomException ex = new CustomException("Вес - это число. Проверь введенные данные.");
                MessageBox.Show(ex.Message, "Внимание!", MessageBoxButton.OKCancel);
            }
        }

        private void Page_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                ButtonNext_Click(sender, e);
                e.Handled = true;
            }
        }

        private void scrollbar_PreviewMouseWheel(object sender, MouseWheelEventArgs e)
        {
            ScrollViewer scv = (ScrollViewer)sender;
            scv.ScrollToVerticalOffset(scv.VerticalOffset - e.Delta);
            e.Handled = true;
        }
    }
}
