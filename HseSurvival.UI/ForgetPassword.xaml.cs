﻿using HseSurvival.Core.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace HseSurvival.UI
{
    /// <summary>
    /// Interaction logic for ForgetPassword.xaml
    /// </summary>
    public partial class ForgetPassword : Window
    {
        public ForgetPassword()
        {
            InitializeComponent();
        }

        private void ButtonConfirm_Click(object sender, RoutedEventArgs e)
        {
            if ((string.IsNullOrWhiteSpace(email_textbox.Text))) MessageBox.Show("Не оставляй поля пустыми!");
            else
            if (DatabaseRepository.IsRegistred(email_textbox.Text) != "not")
            {
                bool sent = DatabaseRepository.ForgotPassword(email_textbox.Text);
                if (!sent) MessageBox.Show("An error occured while sending an email. Check your input and try again", "Error");
                else
                {
                    var confirmWindow = new ConfirmWindow(email_textbox.Text);
                    confirmWindow.Show();
                    this.Close();
                }
            }
            else MessageBox.Show("Эта почта не зарегестрирована. Пройди регистрацию или проверь ввод");
        }
    }
}
