﻿using HseSurvival.Core;
using HseSurvival.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace HseSurvival.UI
{
    /// <summary>
    /// Interaction logic for ProfilePage.xaml
    /// </summary>
    public partial class ProfilePage : Page
    {
        Repository repo = new Repository();
        public ProfilePage()
        {            
            User user = repo.Users.First();
            InitializeComponent();
            textblock_name.Text = user.Name;
            textblock_email.Text = user.Email;
            textbox_group.Text = user.Group;           
        }

        private void Change_Password_Click(object sender, RoutedEventArgs e)
        {
            var forgetPassWindow = new ForgetPassword();
            forgetPassWindow.Show();
        }
    }
}
