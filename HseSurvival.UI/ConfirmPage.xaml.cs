﻿using HseSurvival.Core.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace HseSurvival.UI
{
    /// <summary>
    /// Логика взаимодействия для ConfirmPage.xaml
    /// </summary>
    public partial class ConfirmPage : Page
    {
        Frame frame;
        string email;
        Window confirmWindow;
        public ConfirmPage(Frame _frame, string user_email, Window _confirmWindow)
        {
            InitializeComponent();
            frame = _frame;
            email = user_email;
            confirmWindow = _confirmWindow;
        }

        private void ButtonConfirm_Click(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrWhiteSpace(code_textbox.Text)) MessageBox.Show("Не оставляй поля пустыми!");
            else
            {
                string token = DatabaseRepository.GetTokenFromDB(email);
                if (code_textbox.Text == token)
                {
                    frame.Content = null;
                    frame.Content = new ChangePassPage(email, confirmWindow);
                }
                else MessageBox.Show("Проверь введенный код", "Ошибка");
            }
        }
    }
}
