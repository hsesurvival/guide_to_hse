﻿using HseSurvival.Core;
using HseSurvival.Core.Helpers;
using HseSurvival.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace HseSurvival.UI
{
    /// <summary>
    /// Interaction logic for ActualCalculator.xaml
    /// </summary>
    public partial class ActualCalculator : Page
    {
        private CalculatorHelpers help = new CalculatorHelpers();

        public delegate string MakeItBeDouble(string text);
        MakeItBeDouble delegator;

        private Frame frame;

        private List<FormOfControl> formsOfControl;
        private double examWeight;
        private double aggrGradeWeight;
        public ActualCalculator(Frame _frame, List<FormOfControl> _formsOfControl, double _examWeight, double _aggrGradeWeight)
        {
            InitializeComponent();
            frame = _frame;
            formsOfControl = _formsOfControl;
            examWeight = _examWeight;
            aggrGradeWeight = _aggrGradeWeight;
            for (int i = 0; i < formsOfControl.Count; i++)
            {
                var stackpanel =
                    new StackPanel
                    {
                        Orientation = Orientation.Vertical,
                        VerticalAlignment = VerticalAlignment.Center,
                        HorizontalAlignment = HorizontalAlignment.Center,
                        Margin = new Thickness(10)
                    };
                stackpanel.Children.Add(new TextBlock
                {
                    Text = formsOfControl[i].Name,
                    Margin = new Thickness(0, 0, 0, 10),
                    Foreground = Brushes.CornflowerBlue,
                    FontSize = 30,
                    FontFamily = new FontFamily("Yu Gothic UI Semibold"),
                    TextAlignment = TextAlignment.Center
                });
                stackpanel.Children.Add(new TextBox
                {
                    BorderBrush = Brushes.LightGray,
                    Width = 60,
                    Height = 60,
                    FontSize = 30,
                    FontFamily = new FontFamily("Yu Gothic UI Semibold"),
                    TextAlignment = TextAlignment.Center,
                    VerticalContentAlignment = VerticalAlignment.Center
                });

                stackpanel_formsOfControl.Children.Add(stackpanel);
            }
            if (stackpanel_formsOfControl.Children.Count > 6)
            {
                scrollbar.HorizontalScrollBarVisibility = ScrollBarVisibility.Visible;
            }
        }

        public void UpdateGrades()
        {
            textboxesWithoutValue?.Clear();
            textbox_exam?.Clear();
            textbox_exam.BorderBrush = Brushes.LightGray;
            textbox_result?.Clear();
            for (int i = 0; i < stackpanel_formsOfControl.Children.Count; i++)
            {
                var stackpanel = stackpanel_formsOfControl.Children[i] as StackPanel;
                var textBoxWithGrade = stackpanel.Children[1] as TextBox;

                textBoxWithGrade?.Clear();
                textBoxWithGrade.BorderBrush = Brushes.LightGray;
            }
        }

        List<TextBox> textboxesWithoutValue;
        private void ButtonCount_Click(object sender, RoutedEventArgs e)
        {
            delegator = help.ReplaceDotToComma;
            textboxesWithoutValue = new List<TextBox>(); //добавляем сюда пустые клетки(только накоп)
            double totalWeightWithoutGrade = 0; //weight of not filled grades
            double partsOfAggrGrade = 0;

            try
            {
                for (int i = 0; i < stackpanel_formsOfControl.Children.Count; i++)
                {
                    var stackpanel = stackpanel_formsOfControl.Children[i] as StackPanel;
                    var textblockWithName = stackpanel.Children[0] as TextBlock;
                    var textBoxWithGrade = stackpanel.Children[1] as TextBox;

                    var formOfControl = formsOfControl.FirstOrDefault(f => f.Name == textblockWithName.Text); //нужный вид работы

                    //отедляем заполненные клетки от незаполненных
                    if (!String.IsNullOrEmpty(textBoxWithGrade.Text)) //заполненные
                    {
                        double valueOfGrade = double.Parse(textBoxWithGrade.Text);

                        if (valueOfGrade > 10)
                        {
                            valueOfGrade = double.Parse(delegator(textBoxWithGrade.Text));
                        }
                        partsOfAggrGrade += (formOfControl.Weight / 100) * valueOfGrade;
                    }
                    else if (String.IsNullOrEmpty(textBoxWithGrade.Text) && !String.IsNullOrEmpty(textbox_result.Text)) //незаполненные
                    {
                        textBoxWithGrade.BorderBrush = Brushes.Red;
                        totalWeightWithoutGrade += (formOfControl.Weight / 100);
                        textboxesWithoutValue.Add(textBoxWithGrade);
                    }
                    else if (String.IsNullOrEmpty(textBoxWithGrade.Text) && String.IsNullOrEmpty(textbox_result.Text))
                    {
                        MessageBox.Show("Мы должны знать, какую итоговую оценку ты хочешь, чтобы рассчитать, " +
                            "что ты должен получить за ту или иную работу! Прочитай, пожалуйста, Правила!", "Внимание!", MessageBoxButton.OKCancel);
                    }
                }
                var aggrGrade = partsOfAggrGrade * (aggrGradeWeight / 100); //накоп


                double examGrade = 0;
                if (!String.IsNullOrEmpty(textbox_exam.Text)) //заполненная
                {
                    double partOfExamGrade = double.Parse(textbox_exam.Text); //экзамен еще не умноженный на вес
                    if (partOfExamGrade > 10)
                    {
                        partOfExamGrade = double.Parse(delegator(textbox_exam.Text));
                    }
                    examGrade = partOfExamGrade * (examWeight / 100);

                }
                else if (String.IsNullOrEmpty(textbox_exam.Text) && !String.IsNullOrEmpty(textbox_result.Text)) //не заполненная
                {
                    textbox_exam.BorderBrush = Brushes.Red;
                }
                else if (String.IsNullOrEmpty(textbox_exam.Text) && String.IsNullOrEmpty(textbox_result.Text))
                {
                    MessageBox.Show("Мы должны знать, какую итоговую оценку ты хочешь, чтобы рассчитать, " +
                       "что ты должен получить за ту или иную работу! Прочитай, пожалуйста, Правила!", "Внимание!", MessageBoxButton.OKCancel);
                }

                if (!String.IsNullOrEmpty(textbox_result.Text))
                {
                    double totalResult = double.Parse(textbox_result.Text);
                    if (totalResult > 10)
                    {
                        totalResult = double.Parse(delegator(textbox_result.Text));
                    }

                    if (!String.IsNullOrEmpty(textbox_exam.Text) && textboxesWithoutValue != null) //только пустой накоп
                    {
                        var result = help.CalculateResultWithEmptyPartOfAggrGrade(totalResult, examGrade, aggrGrade, totalWeightWithoutGrade);
                        foreach (var textbox in textboxesWithoutValue)
                        {
                            textbox.Text = result.ToString();
                        }

                    }
                    else if (String.IsNullOrEmpty(textbox_exam.Text) && textboxesWithoutValue == null) //только пустой экзамен
                    {
                        var result = (totalResult - aggrGrade) / (examWeight / 100);
                    }
                    else if (textboxesWithoutValue != null && String.IsNullOrEmpty(textbox_exam.Text)) //пустые и экзамен и что-то из накопа
                    {
                        var result = help.CalculateResultWithEmptyExamAndAggrGrades(totalResult, aggrGrade, examWeight, aggrGradeWeight, totalWeightWithoutGrade);
                        foreach (var textbox in textboxesWithoutValue)
                        {
                            textbox.Text = result.ToString();
                        }
                        textbox_exam.Text = result.ToString();
                    }
                }
                else
                {
                    textbox_result.Text = (aggrGrade + examGrade).ToString();
                }
            }
            catch
            {
                CustomException c = new CustomException("Оценка - это число. Проверь пожалуйста введенные данные.");
                MessageBox.Show(c.Message, "Внимание!", MessageBoxButton.OKCancel);
                UpdateGrades();
            }
        }

        private void ButtonBack_Click(object sender, RoutedEventArgs e)
        {
            frame.Content = new CalculatorPage(frame);
        }

        private void ButtonRefresh_Click(object sender, RoutedEventArgs e)
        {
            UpdateGrades();
        }

        private void Page_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                ButtonCount_Click(sender, e);
                e.Handled = true;
            }
        }

        private void ScrollViewer_PreviewMouseWheel(object sender, MouseWheelEventArgs e)
        {
            ScrollViewer scv = (ScrollViewer)sender;
            scv.ScrollToHorizontalOffset(scv.HorizontalOffset - e.Delta);
            e.Handled = true;
        }
    }
}
