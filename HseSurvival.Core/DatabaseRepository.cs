﻿using HseSurvival.Core.Models;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HseSurvival.Core.Helpers
{
    public class DatabaseRepository
    {
        public MySqlConnection Connection()
        {
            string conStr = "server=localhost;user=root;database=hse_survival;password=;SslMode=none;CharSet=utf8;";
            MySqlConnection con = new MySqlConnection(conStr);
            return con;
        }
        public List<Discipline> LoadData(string op)
        {
            var conn = Connection();
            conn.Open();
            // запрос

            string sql = "SELECT * FROM disciplines WHERE disciplines.EduProgram=?edu";
            MySqlCommand command = conn.CreateCommand();
            command.CommandText = sql;
            command.Parameters.AddWithValue("?edu", op);
            MySqlDataReader reader = command.ExecuteReader();

            List<Discipline> disciplines = new List<Discipline>();
            while (reader.Read())
            {
                disciplines.Add(new Discipline
                {
                    Name = reader["Name"].ToString(),
                    EduProgram = reader["EduProgram"].ToString(),
                    LinkToPDF = reader["LinkToPDF"].ToString(),
                    Credits = int.Parse(reader["Credits"].ToString())
                });
            }

            disciplines.Remove(disciplines.FirstOrDefault(item => item.Name.Contains("экзамен по английскому языку")));
            disciplines.Remove(disciplines.FirstOrDefault(item => item.Name.Contains("Физическая")));
            // закрываем соединение с БД
            conn.Close();
            return disciplines;


        }
        public List<Discipline> LoadDataForCalc(string op)
        {

            var conn = Connection();
            conn.Open();
            // запрос
            string sql = "SELECT * FROM disciplines WHERE disciplines.EduProgram=?edu";
            MySqlCommand command = conn.CreateCommand();
            command.CommandText = sql;
            command.Parameters.AddWithValue("?edu", op);
            MySqlDataReader reader = command.ExecuteReader();

            List<Discipline> disciplines = new List<Discipline>();
            while (reader.Read())
            {
                disciplines.Add(new Discipline
                {

                    Name = reader["Name"].ToString(),
                    ExamWeight = double.Parse(reader["ExamWeight"].ToString()),
                    AccumWeight = double.Parse(reader["AccumWeight"].ToString()),
                    FormsOfControl = LoadForms(int.Parse(reader["Id"].ToString()))

                });

            }
            disciplines.Remove(disciplines.FirstOrDefault(item => item.Name.Contains("экзамен по английскому языку")));
            disciplines.Remove(disciplines.FirstOrDefault(item => item.Name.Contains("Физическая")));

            // закрываем соединение с БД
            conn.Close();
            return disciplines;


        }
        public List<FormOfControl> LoadForms(int id)
        {
            var conn = Connection();
            conn.Open();
            //
            string sql = "SELECT * from formofcontrol where formofcontrol.Id IN (SELECT disciplines_formsofcontrol.FormOfControl_Id from disciplines_formsofcontrol where disciplines_formsofcontrol.Discipline_Id = ?id)";
            MySqlCommand command = conn.CreateCommand();
            command.CommandText = sql;
            command.Parameters.AddWithValue("?id", id);
            MySqlDataReader reader = command.ExecuteReader();

            List<FormOfControl> forms = new List<FormOfControl>();
            while (reader.Read())
            {
                forms.Add(new FormOfControl
                {
                    Weight = double.Parse(reader["Weight"].ToString()),
                    Name = reader["Name"].ToString(),
                    HasDigits = bool.Parse(reader["HasDigits"].ToString()),
                    PlusWeight = double.Parse(reader["PlusWeight"].ToString())
                });
            }
            //
            conn.Close();
            return forms;
        }
    }
}
