﻿namespace HseSurvival.Core.Models
{
    public class FormOfControl
    {
        public int ID { get; set; }
        public double Weight { get; set; }
        public string Name { get; set; }
        public bool HasDigits { get; set; } // false если плюсы вместо оценок
        public double PlusWeight { get; set; } // вес одного плюса, если HasDigits == false

    }
}