﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HseSurvival.Core.Models
{
    public class Discipline
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public int Credits { get; set; }
        public double ExamWeight { get; set; }
        public double AccumWeight { get; set; } 
        public List<FormOfControl> FormsOfControl { get; set; }
        public string EduProgram { get; set; }
        public string LinkToPDF { get; set; }
    }
}
