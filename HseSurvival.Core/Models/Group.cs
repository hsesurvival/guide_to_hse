﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HseSurvival.Core.Models
{
    public class Group
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string EduProgram { get; set; }
    }
}
