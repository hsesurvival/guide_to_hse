﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HseSurvival.Core.Models
{
    class Formula : BaseFormula
    {
        //public int Mkr { get; set; }
        //public int Kdz { get; set; }
        //public int Kr { get; set; }
        //public int Aud { get; set; }
        //public int AggregatedGradeWeight { get; set; }
        //public int ExamWeight { get; set; }

        public Formula(int mkr, int kdz, int kr, int aud, int aggrGrade, int exam)
        {
            Mkr = mkr;
            Kdz = kdz;
            Kr = kr;
            Aud = aud;
            AggregatedGradeWeight = aggrGrade;
            ExamWeight = exam;
        }
    }
}
