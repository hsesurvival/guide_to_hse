﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HseSurvival.Core.Models
{
    public class User
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string Group { get; set; }
        //public Discipline ElectiveCourse { get; set; }
        //public Discipline Minor { get; set; } // + у гуманитариев миноры
        //public Discipline OptionalCourse { get; set; }

    }
}
