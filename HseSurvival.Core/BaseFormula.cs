﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HseSurvival.Core
{
    abstract class BaseFormula
    {
        public virtual int Mkr { get; set; }
        public virtual int Kdz { get; set; }
        public virtual int Kr { get; set; }
        public virtual int Aud { get; set; }
        public virtual int TeamProject { get; set; }
        public virtual int AggregatedGradeWeight { get; set; }
        public virtual int ExamWeight { get; set; }
    }
}
