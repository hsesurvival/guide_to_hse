﻿using System;
using System.Net;
using System.IO;
using HtmlAgilityPack;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

class HtmlParsing

{
    static void Main()
    {
        HtmlWeb www = new HtmlWeb();
        HtmlDocument doc = www.Load(@"https://www.hse.ru/edu/courses/index.html?words=&full_words=&edu_year=2017&lecturer=&edu_level=&language=&level=1191462%3A130671204&mandatory=1&mandatory=2&is_dpo=0&filial=22723&department=143571564&modules=1191495&modules=1191496&modules=1191497&modules=1191498&xlc=&genelective=-1");

        var sections = doc.DocumentNode.SelectNodes("//div[@class='b-program']");
        foreach (var section in sections)
        {
            string text = section.InnerText.ToString();
            var info = text.Split('\n');

            for (int i = 0; i < info.Length; i++)
            {
                if (info[i] != "\n")
                    info[i] = info[i].Split(':').Last();
            }


            // название дисциплины
            string name = info[2]; // прописать if для английского и физры (иначе в оп будет муть)

            // количество кредитов credits
            string creditsWrongFormat = info[14];
            int credits;
            if (string.IsNullOrEmpty(creditsWrongFormat))
                credits = 0;
            else
                credits = (int)char.GetNumericValue(creditsWrongFormat[1]);

            // преподаватели lecturers
            var lecturersWrongFormat = info[7].Split(',');
            List<string> lecturers = new List<string>();
            foreach (var item in lecturersWrongFormat)
            {
                string itemFormatted = item.Split('(')[0].TrimEnd(' ').TrimStart(' ');
                if (itemFormatted.StartsWith("ведет") != true)
                    lecturers.Add(itemFormatted);
            }

            // образовательная программа
            string eduProgram = info[12].Split('.')[3].TrimStart(' '); // найти все сокращения

            // создание url для загрузки программы дисциплины linkToPdf
            Regex pdf = new Regex(@"/\d{2}/\d{2}/\d{10}/program-\d{10}-\w+.pdf");
            string[] urlParts = { "https://www.hse.ru/data/2018", pdf.Match(section.InnerHtml).ToString() };
            string linkToPdf = string.Join("", urlParts);

        }
    }
}
