﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Caching;
using System.Text;
using System.Threading.Tasks;

namespace HseSurvival.Core.Helpers
{
    public class Caching
    {
        private static readonly ObjectCache Cache = MemoryCache.Default;

        public static T Get<T>(string key) where T : class
        {
            try
            {
                return (T)Cache[key];
            }
            catch
            {
                return null;
            }
        }

        public static void Add<T>(string key, T item) where T : class
        {
            Cache.Add(key, item, DateTimeOffset.Now.AddMinutes(10));
        }
    }
}
