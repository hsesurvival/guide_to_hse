﻿using HseSurvival.Core.Models;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace HseSurvival.Core.Helpers
{
    public class DatabaseRepository
    {
        public static MySqlConnection Connection()
        {
            string conStr = "server=localhost;user=root;database=hse_survival;password=;SslMode=none;CharSet=utf8;";
            MySqlConnection con = new MySqlConnection(conStr);
            return con;
        }
        public static string GetHash(string password)
        {
            var md5 = MD5.Create();
            var hash = md5.ComputeHash(Encoding.UTF8.GetBytes(
            password));
            return Convert.ToBase64String(hash);
        }

        public List<Discipline> LoadData(string op)
        {
            var conn = Connection();
            conn.Open();
            // запрос

            string sql = "SELECT * FROM disciplines WHERE disciplines.EduProgram=?edu";
            MySqlCommand command = conn.CreateCommand();
            command.CommandText = sql;
            command.Parameters.AddWithValue("?edu", op);
            MySqlDataReader reader = command.ExecuteReader();

            List<Discipline> disciplines = new List<Discipline>();
            while (reader.Read())
            {
                disciplines.Add(new Discipline
                {
                    Name = reader["Name"].ToString(),
                    EduProgram = reader["EduProgram"].ToString(),
                    LinkToPDF = reader["LinkToPDF"].ToString(),
                    Credits = int.Parse(reader["Credits"].ToString())
                });
            }

            disciplines.Remove(disciplines.FirstOrDefault(item => item.Name.Contains("экзамен по английскому языку")));
            disciplines.Remove(disciplines.FirstOrDefault(item => item.Name.Contains("Физическая")));
            // закрываем соединение с БД
            conn.Close();
            return disciplines;


        }
        public List<Discipline> LoadDataForCalc(string op)
        {

            var conn = Connection();
            conn.Open();
            // запрос
            string sql = "SELECT * FROM disciplines WHERE disciplines.EduProgram=?edu";
            MySqlCommand command = conn.CreateCommand();
            command.CommandText = sql;
            command.Parameters.AddWithValue("?edu", op);
            MySqlDataReader reader = command.ExecuteReader();

            List<Discipline> disciplines = new List<Discipline>();
            while (reader.Read())
            {
                disciplines.Add(new Discipline
                {

                    Name = reader["Name"].ToString(),
                    ExamWeight = double.Parse(reader["ExamWeight"].ToString()),
                    AccumWeight = double.Parse(reader["AccumWeight"].ToString()),
                    FormsOfControl = LoadForms(int.Parse(reader["Id"].ToString()))

                });

            }
            disciplines.Remove(disciplines.FirstOrDefault(item => item.Name.Contains("экзамен по английскому языку")));
            disciplines.Remove(disciplines.FirstOrDefault(item => item.Name.Contains("Физическая")));
            disciplines = disciplines.Where(item => item.AccumWeight != 0).ToList();

            // закрываем соединение с БД
            conn.Close();
            return disciplines;


        }
        public List<FormOfControl> LoadForms(int id)
        {
            var conn = Connection();
            conn.Open();
            //
            string sql = "SELECT * from formofcontrol where formofcontrol.Id IN (SELECT disciplines_formsofcontrol.FormOfControl_Id from disciplines_formsofcontrol where disciplines_formsofcontrol.Discipline_Id = ?id)";
            MySqlCommand command = conn.CreateCommand();
            command.CommandText = sql;
            command.Parameters.AddWithValue("?id", id);
            MySqlDataReader reader = command.ExecuteReader();

            List<FormOfControl> forms = new List<FormOfControl>();
            while (reader.Read())
            {
                forms.Add(new FormOfControl
                {
                    Weight = double.Parse(reader["Weight"].ToString()),
                    Name = reader["Name"].ToString(),
                    HasDigits = bool.Parse(reader["HasDigits"].ToString()),
                    PlusWeight = double.Parse(reader["PlusWeight"].ToString())
                });
            }
            //
            conn.Close();
            return forms;
        }
        private static string RandomToken()
        {
            int length = 8;


            const string valid = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
            Random rng = new Random();
            StringBuilder res = new StringBuilder();

            for (int i = 0; i < length; i++)
            {
                res.Append(valid[rng.Next(valid.Length)]);
            }
            string code = res.ToString();
            return code;
        }
        public static string IsRegistred(string user_email)
        {

            string password;
            string email = user_email + "@edu.hse.ru";
            var conn = Connection();
            conn.Open();
            MySqlCommand command = conn.CreateCommand();
            command.CommandText = "SELECT Password from users where users.Email= ?email";
            command.Parameters.AddWithValue("?email", email);
            try
            {
                password = command.ExecuteScalar().ToString();
            }
            catch
            {
                conn.Close();
                return "not";
            }
            conn.Close();
            return password;
        }

        public static bool ForgotPassword(string user_email)
        {
            string email = user_email + "@edu.hse.ru";
            string code = RandomToken();
            var conn = Connection();
            conn.Open();
            MySqlCommand command = conn.CreateCommand();
            command.CommandText = "UPDATE users SET Reset_token=?code WHERE Email=?email";
            command.Parameters.AddWithValue("?code", code);
            command.Parameters.AddWithValue("?email", email);
            command.ExecuteNonQuery();
            conn.Close();

            //Console.WriteLine("work with bd successfull");
            try
            {
                MailMessage mail = new MailMessage();
                SmtpClient SmtpServer = new SmtpClient("smtp.yandex.ru");

                mail.From = new MailAddress("hse.survival@yandex.ru");
                mail.To.Add(email);
                mail.Subject = "Сброс пароля";

                mail.IsBodyHtml = true;
                string htmlBody;
                string[] htmlparts = { WebUtility.HtmlEncode(@"<head> <meta http-equiv=""Content-Type"" content=""text/html; charset=utf-8""> <meta name=""viewport"" content=""width=device-width""> <title>HSE: A Guide For Survival</title> <style type = ""text/css""> @media(min - width: 500px) { .avatar__media.media__fluid { margin - top: 3px !important; } } @media(min - width: 500px) { .button, .button__shadow { font - size: 16px !important; display: inline - block !important; width: auto !important; } } @media(min - width: 500px) { footer li { display: inline - block !important; margin - right: 20px !important; } } @media(min - width: 500px) { .mt1--lg { margin - top: 10px !important; } } @media(min - width: 500px) { .mt2--lg { margin - top: 20px !important; } } @media(min - width: 500px) { .mt3--lg { margin - top: 30px !important; } } @media(min - width: 500px) { .mt4--lg { margin - top: 40px !important; } } @media(min - width: 500px) { .mb1--lg { margin - bottom: 10px !important; } } @media(min - width: 500px) { .mb2--lg { margin - bottom: 20px !important; } } @media(min - width: 500px) { .mb3--lg { margin - bottom: 30px !important; } } @media(min - width: 500px) { .mb4--lg { margin - bottom: 40px !important; } } @media(min - width: 500px) { .pt1--lg { padding - top: 10px !important; } } @media(min - width: 500px) { .pt2--lg { padding - top: 20px !important; } } @media(min - width: 500px) { .pt3--lg { padding - top: 30px !important; } } @media(min - width: 500px) { .pt4--lg { padding - top: 40px !important; } } @media(min - width: 500px) { .pb1--lg { padding - bottom: 10px !important; } } @media(min - width: 500px) { .pb2--lg { padding - bottom: 20px !important; } } @media(min - width: 500px) { .pb3--lg { padding - bottom: 30px !important; } } @media(min - width: 500px) { .pb4--lg { padding - bottom: 40px !important; } } @media(min - width: 500px) { pre { font - size: 14px !important; } .body { font - size: 14px !important; line - height: 24px !important; } h1 { font - size: 22px !important; } h2 { font - size: 16px !important; } small { font - size: 12px !important; } } @media(min - width: 500px) { .user - content pre, .user - content code { font - size: 14px !important; line - height: 24px !important; } .user - content ul, .user - content ol, .user - content pre { margin - top: 12px !important; margin - bottom: 12px !important; } .user - content hr { margin: 12px 0 !important; } .user - content h1 { font - size: 22px !important; } .user - content h2 { font - size: 16px !important; } .user - content h3 { font - size: 14px !important; } } </style> <h1 style = ""box-sizing: border-box; font-size: 1.25rem; margin: 0; margin-bottom: 0.5em; padding: 0;""> Спасибо, что пользуешься нашим приложением!</h1> </head > <body class=""body"" style=""font-family: -apple-system, BlinkMacSystemFont, Roboto, Ubuntu, Helvetica, sans-serif; line-height: initial; max-width: 580px;""> <header class=""mt2 mb2"" style=""margin-bottom: 20px; margin-top: 20px;""> <p style = ""box-sizing: border-box; margin: 0; margin-bottom: 0.5em; padding: 0;""> Введи этот код для восстановления пароля или проигнорируй это письмо, если не знаешь, о чём мы.</p> </header> <p style = ""box-sizing: border-box; margin: 0; margin-bottom: 0.5em; padding: 0;""> </p><pre style=""background: #f6f6f3; border: 1px solid #d8d7d4; border-radius: 3px; display: inline-block; font-family: monospace; font-size: 1.25rem; font-weight: 600; line-height: 1em; margin: 0; padding: 5px 8px;"">"), code, WebUtility.HtmlEncode(@"</pre> <p></p> </body>") };
                htmlBody = string.Join("", htmlparts);

                mail.IsBodyHtml = true;
                mail.Body = WebUtility.HtmlDecode(htmlBody);

                SmtpServer.Port = 587;
                SmtpServer.Credentials = new System.Net.NetworkCredential("hse.survival", "b6b8096e577b1800baa89c74f3915977");
                SmtpServer.EnableSsl = true;

                SmtpServer.Send(mail);
                //Console.WriteLine("Email sent!");
                return true;
            }
            catch (Exception ex)
            {
                return false;
                //Console.WriteLine(ex.ToString());
            }

        }

        public static bool ConfirmEmail(string user_email, string name, string group_name)
        {
            string email = user_email + "@edu.hse.ru";
            string code = RandomToken();
            var conn = Connection();
            conn.Open();
            MySqlCommand command = conn.CreateCommand();
            command.CommandText = "INSERT INTO users (Id, Name, Email, Group_Name, Reset_token,EduProgram) VALUES ('', ?name, ?email, ?group_name, ?code,'Бизнес-информатика')";
            command.Parameters.AddWithValue("?name", name);
            command.Parameters.AddWithValue("?group_name", group_name);
            command.Parameters.AddWithValue("?code", code);
            command.Parameters.AddWithValue("?email", email);
            command.ExecuteNonQuery();
            conn.Close();

            //Console.WriteLine("work with bd successfull");
            try
            {
                MailMessage mail = new MailMessage();
                SmtpClient SmtpServer = new SmtpClient("smtp.yandex.ru");

                mail.From = new MailAddress("hse.survival@yandex.ru");
                mail.To.Add(email);
                mail.Subject = "Сброс пароля";

                mail.IsBodyHtml = true;
                string htmlBody;
                string[] htmlparts = { WebUtility.HtmlEncode(@"<head> <meta http-equiv=""Content-Type"" content=""text/html; charset=utf-8""> <meta name=""viewport"" content=""width=device-width""> <title>HSE: A Guide For Survival</title> <style type=""text/css""> @media (min-width: 500px) { .avatar__media .media__fluid { margin-top: 3px !important; } } @media (min-width: 500px) { .button, .button__shadow { font-size: 16px !important; display: inline-block !important; width: auto !important; } } @media (min-width: 500px) { footer li { display: inline-block !important; margin-right: 20px !important; } } @media (min-width: 500px) { .mt1--lg { margin-top: 10px !important; } } @media (min-width: 500px) { .mt2--lg { margin-top: 20px !important; } } @media (min-width: 500px) { .mt3--lg { margin-top: 30px !important; } } @media (min-width: 500px) { .mt4--lg { margin-top: 40px !important; } } @media (min-width: 500px) { .mb1--lg { margin-bottom: 10px !important; } } @media (min-width: 500px) { .mb2--lg { margin-bottom: 20px !important; } } @media (min-width: 500px) { .mb3--lg { margin-bottom: 30px !important; } } @media (min-width: 500px) { .mb4--lg { margin-bottom: 40px !important; } } @media (min-width: 500px) { .pt1--lg { padding-top: 10px !important; } } @media (min-width: 500px) { .pt2--lg { padding-top: 20px !important; } } @media (min-width: 500px) { .pt3--lg { padding-top: 30px !important; } } @media (min-width: 500px) { .pt4--lg { padding-top: 40px !important; } } @media (min-width: 500px) { .pb1--lg { padding-bottom: 10px !important; } } @media (min-width: 500px) { .pb2--lg { padding-bottom: 20px !important; } } @media (min-width: 500px) { .pb3--lg { padding-bottom: 30px !important; } } @media (min-width: 500px) { .pb4--lg { padding-bottom: 40px !important; } } @media (min-width: 500px) { pre { font-size: 14px !important; } .body { font-size: 14px !important; line-height: 24px !important; } h1 { font-size: 22px !important; } h2 { font-size: 16px !important; } small { font-size: 12px !important; } } @media (min-width: 500px) { .user-content pre, .user-content code { font-size: 14px !important; line-height: 24px !important; } .user-content ul, .user-content ol, .user-content pre { margin-top: 12px !important; margin-bottom: 12px !important; } .user-content hr { margin: 12px 0 !important; } .user-content h1 { font-size: 22px !important; } .user-content h2 { font-size: 16px !important; } .user-content h3 { font-size: 14px !important; } } </style> <h1 style=""box-sizing: border-box; font-size: 1.25rem; margin: 0; margin-bottom: 0.5em; padding: 0;"">Спасибо за установку!</h1> </head> <body class=""body"" style=""font-family: -apple-system, BlinkMacSystemFont, Roboto, Ubuntu, Helvetica, sans-serif; line-height: initial; max-width: 580px;""> <header class=""mt2 mb2"" style=""margin-bottom: 20px; margin-top: 20px;""> <p style=""box-sizing: border-box; margin: 0; margin-bottom: 0.5em; padding: 0;""> Привет! Ты зарегистрировался в приложении <b> HSE: A Guide For Survival</b>. Пожалуйста, подтверди свой адрес или проигнорируй это письмо, если не знаешь, о чём мы.</p> </header> <p style=""box-sizing: border-box; margin: 0; margin-bottom: 0.5em; padding: 0;""> </p><pre style=""background: #f6f6f3; border: 1px solid #d8d7d4; border-radius: 3px; display: inline-block; font-family: monospace; font-size: 1.25rem; font-weight: 600; line-height: 1em; margin: 0; padding: 5px 8px;"">"), code, WebUtility.HtmlEncode(@"</pre> <p></p> </body>") };
                htmlBody = string.Join("", htmlparts);

                mail.IsBodyHtml = true;
                mail.Body = WebUtility.HtmlDecode(htmlBody);

                SmtpServer.Port = 587;
                SmtpServer.Credentials = new System.Net.NetworkCredential("hse.survival", "b6b8096e577b1800baa89c74f3915977");
                SmtpServer.EnableSsl = true;

                SmtpServer.Send(mail);
                //Console.WriteLine("Email sent!");
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public static string GetTokenFromDB(string user_email)
        {
            string email = user_email + "@edu.hse.ru";
            var conn = Connection();
            conn.Open();
            MySqlCommand command = conn.CreateCommand();
            command.CommandText = "SELECT Reset_token from users where users.Email= ?email";
            command.Parameters.AddWithValue("?email", email);
            string token = command.ExecuteScalar().ToString();
            conn.Close();
            return token;
        }

        public static void ResetPassword(string new_password, string user_email)
        {
            string email = user_email + "@edu.hse.ru";
            string password = GetHash(new_password);
            var conn = Connection();
            conn.Open();
            MySqlCommand command = conn.CreateCommand();
            command.CommandText = "UPDATE users SET Password = ?password, Reset_token = NULL WHERE users.Email =?email";
            command.Parameters.AddWithValue("?email", email);
            command.Parameters.AddWithValue("?password", password);
            command.ExecuteNonQuery();
            conn.Close();
        }
        public static void UpdateGroup(string user_email, string group)
        {
            string email = user_email + "@edu.hse.ru";
            var conn = Connection();
            conn.Open();
            MySqlCommand command = conn.CreateCommand();
            command.CommandText = "UPDATE users SET Group_Name = ?group WHERE users.Email =?email";
            command.Parameters.AddWithValue("?email", email);
            command.Parameters.AddWithValue("?group", group);
            command.ExecuteNonQuery();
            conn.Close();
        }
    }
}
