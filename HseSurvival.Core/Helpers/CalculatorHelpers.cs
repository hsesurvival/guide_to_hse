﻿using HseSurvival.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HseSurvival.Core.Helpers
{
    public class CalculatorHelpers
    {
        public string ReplaceDotToComma(string text)
        {
            var comma = ",";
            var dot = ".";
            if (text.Contains(comma))
            {
                text = text.Replace(comma, dot);
            }
            else if (text.Contains(dot))
            {
                text = text.Replace(dot, comma);
            }
            return text;
        }


        public double CalculateAccumGrade(FormOfControl formOfControl, double partOfAggreGrade, double partsOfAggrGrade)
        {
            if (formOfControl.HasDigits != false)
            {
                partsOfAggrGrade += (formOfControl.Weight / 100) * partOfAggreGrade;
            }
            else
            {
                partsOfAggrGrade += (formOfControl.Weight / 100) * partOfAggreGrade * formOfControl.PlusWeight;
            }
            return partsOfAggrGrade;
        }

        public double CalculateResultWithEmptyPartOfAggrGrade(double totalResult, double examGrade, double aggrGrade, double totalWeightWithoutGrade)
        {
            var neededAmount = totalResult - examGrade; //сколько надо дополучить в накопе
            return (neededAmount - aggrGrade) / totalWeightWithoutGrade;
        }

        public double CalculateResultWithEmptyExamAndAggrGrades(double totalResult, double aggrGrade, double examWeight, double aggrGradeWeight, double totalWeightWithoutGrade)
        {
            var partWithX = totalWeightWithoutGrade * (aggrGradeWeight / 100) + examWeight / 100;
            var partWithoutX = totalResult - aggrGrade;
            return partWithoutX / partWithX;
        }
    }
}
