﻿using HseSurvival.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HseSurvival.Core
{
    public class Repository
    {
        public List<User> Users { get; set; }
        public List<Discipline> Disciplines { get; set; }
        public List<FormOfControl> FormsOfControl { get; set; }

        public Repository()
        {
            Users = new List<User>
            {
                new User
                {
                    Name = "Nga",
                    Email = "tvu_1@edu.hse.ru",
                    Group = "BBI177"
                }
            };

            FormsOfControl = new List<FormOfControl>
            {
                new FormOfControl {
                    Name = "Kdz",
                    Weight = 40,
                    HasDigits = true,
                    PlusWeight = 0
                },
                new FormOfControl {
                    Name = "Kr",
                    Weight = 40,
                    HasDigits = true,
                    PlusWeight = 0
                },
                new FormOfControl {
                    Name = "Mkr",
                    Weight = 70,
                    HasDigits = true,
                    PlusWeight = 0
                },
                new FormOfControl {
                    Name = "Aud",
                    Weight = 30,
                    HasDigits = false,
                    PlusWeight = 0.1
                },
                new FormOfControl {
                    Name = "Tp",
                    Weight = 30,
                    HasDigits = true,
                    PlusWeight = 0
                },
                new FormOfControl {
                    Name = "Test",
                    Weight = 30,
                    HasDigits = true,
                    PlusWeight = 0
                }
               
            };
            Disciplines = new List<Discipline>
            {
                new Discipline {
                    Name = "Programming",
                    ExamWeight = 40,
                    AccumWeight = 60,
                    FormsOfControl = new List<FormOfControl>
                    {
                        new FormOfControl
                        {
                            Name = FormsOfControl[0].Name,
                            Weight = FormsOfControl[0].Weight,
                            HasDigits = FormsOfControl[0].HasDigits,
                            PlusWeight = FormsOfControl[0].PlusWeight
                        }, 
                        new FormOfControl
                        {
                            Name = FormsOfControl[4].Name,
                            Weight = FormsOfControl[4].Weight,
                            HasDigits = FormsOfControl[4].HasDigits,
                            PlusWeight = FormsOfControl[4].PlusWeight
                        },
                        new FormOfControl
                        {
                            Name = FormsOfControl[5].Name,
                            Weight = FormsOfControl[5].Weight,
                            HasDigits = FormsOfControl[5].HasDigits,
                            PlusWeight = FormsOfControl[5].PlusWeight
                        }
                    }
                },
                new Discipline
                {
                    Name = "Calculus",
                    ExamWeight = 75,
                    AccumWeight = 25,
                    FormsOfControl = new List<FormOfControl>
                    {
                        new FormOfControl
                        {
                            Name = FormsOfControl[2].Name,
                            Weight = FormsOfControl[2].Weight,
                            HasDigits = FormsOfControl[2].HasDigits,
                            PlusWeight = FormsOfControl[2].PlusWeight
                        },
                        new FormOfControl
                        {
                            Name = FormsOfControl[3].Name,
                            Weight = FormsOfControl[3].Weight,
                            HasDigits = FormsOfControl[3].HasDigits,
                            PlusWeight = FormsOfControl[3].PlusWeight
                        }
                    }
                }
            };
        }
    }
}
