-- phpMyAdmin SQL Dump
-- version 4.7.3
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Июн 12 2018 г., 02:00
-- Версия сервера: 5.6.37
-- Версия PHP: 5.5.38

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `hse_survival`
--

-- --------------------------------------------------------

--
-- Структура таблицы `disciplines`
--

CREATE TABLE `disciplines` (
  `Id` int(11) NOT NULL,
  `Name` varchar(50) NOT NULL,
  `Credits` int(5) NOT NULL,
  `EduProgram` varchar(50) NOT NULL,
  `LinkToPDF` varchar(50) NOT NULL,
  `AccumWeight` double NOT NULL,
  `ExamWeight` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `disciplines`
--

INSERT INTO `disciplines` (`Id`, `Name`, `Credits`, `EduProgram`, `LinkToPDF`, `AccumWeight`, `ExamWeight`) VALUES
(1, 'Алгебра и геометрия', 6, 'Бизнес-информатика', 'https://www.hse.ru/data/2018/01/26/1162909016/prog', 0.6, 0.4),
(2, 'Дискретная математика', 8, 'Бизнес-информатика', 'https://www.hse.ru/data/2018/01/26/1162936745/prog', 0.6, 0.4),
(3, 'История', 4, 'Бизнес-информатика', 'https://www.hse.ru/data/2018/01/26/1162938437/prog', 0.4, 0.6),
(4, 'Математический анализ', 8, 'Бизнес-информатика', 'https://www.hse.ru/data/2018/01/26/1162918111/prog', 0.6, 0.4),
(5, 'Проектный семинар', 3, 'Бизнес-информатика', 'https://www.hse.ru/data/2018/01/26/1162913586/prog', 0, 0),
(6, 'Теоретические основы информатики', 6, 'Бизнес-информатика', 'https://www.hse.ru/data/2018/01/26/1162928481/prog', 0.3, 0.7),
(7, 'Философия', 4, 'Бизнес-информатика', 'https://www.hse.ru/data/2018/01/26/1162906673/prog', 0.5, 0.5),
(8, 'Экономика', 4, 'Бизнес-информатика', 'https://www.hse.ru/data/2018/01/26/1162922462/prog', 0.5, 0.5);

-- --------------------------------------------------------

--
-- Структура таблицы `disciplines_formsofcontrol`
--

CREATE TABLE `disciplines_formsofcontrol` (
  `Discipline_Id` int(11) NOT NULL,
  `FormOfControl_Id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `disciplines_formsofcontrol`
--

INSERT INTO `disciplines_formsofcontrol` (`Discipline_Id`, `FormOfControl_Id`) VALUES
(1, 1),
(1, 1),
(1, 2),
(1, 3),
(1, 4),
(2, 5),
(2, 6),
(3, 7),
(3, 8),
(4, 9),
(4, 10),
(4, 11),
(4, 12),
(6, 13),
(6, 14),
(6, 15),
(7, 16),
(7, 17),
(8, 18),
(8, 19);

-- --------------------------------------------------------

--
-- Структура таблицы `formofcontrol`
--

CREATE TABLE `formofcontrol` (
  `Id` int(11) NOT NULL,
  `Weight` double NOT NULL,
  `Name` varchar(50) NOT NULL,
  `HasDigits` tinyint(1) NOT NULL DEFAULT '1',
  `PlusWeight` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `formofcontrol`
--

INSERT INTO `formofcontrol` (`Id`, `Weight`, `Name`, `HasDigits`, `PlusWeight`) VALUES
(1, 0.35, 'КР1', 1, 0),
(2, 0.35, 'КР2', 1, 0),
(3, 0.2, 'КДЗ', 1, 0),
(4, 0.1, 'АУД', 1, 0),
(5, 0.3, 'КР', 1, 0),
(6, 0.1, 'МКР', 1, 0),
(7, 0.2, 'ТЕСТ', 1, 0),
(8, 0.8, 'АУД', 1, 0),
(9, 0.28, 'МКР1', 1, 0),
(10, 0.07, 'АУД1', 1, 0),
(11, 0.4875, 'МКР1', 1, 0),
(12, 0.1625, 'АУД1', 1, 0),
(13, 0.28571429, 'КДЗ', 1, 0),
(14, 0.28571429, 'АУД', 1, 0),
(15, 0.42857143, 'КР', 1, 0),
(16, 0.5, 'АУД', 0, 0.83333333),
(17, 0.5, 'ЭССЕ', 1, 0),
(18, 0.4, 'АУД', 1, 0),
(19, 0.6, 'КР', 1, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `groups`
--

CREATE TABLE `groups` (
  `Id` int(11) NOT NULL,
  `Name` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `Groups_disciplines`
--

CREATE TABLE `Groups_disciplines` (
  `Group_Id` int(11) NOT NULL,
  `Discipline_Id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `Groups_disciplines`
--

INSERT INTO `Groups_disciplines` (`Group_Id`, `Discipline_Id`) VALUES
(1, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE `users` (
  `Id` int(11) NOT NULL,
  `Name` varchar(20) NOT NULL,
  `Email` varchar(50) NOT NULL,
  `Password` varchar(50) NOT NULL,
  `Group_Id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`Id`, `Name`, `Email`, `Password`, `Group_Id`) VALUES
(1, 'Rodya', 'rodion2030@gmail.com', 'hehe', 177);

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `disciplines`
--
ALTER TABLE `disciplines`
  ADD PRIMARY KEY (`Id`);

--
-- Индексы таблицы `formofcontrol`
--
ALTER TABLE `formofcontrol`
  ADD PRIMARY KEY (`Id`);

--
-- Индексы таблицы `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`Id`);

--
-- Индексы таблицы `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`Id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `disciplines`
--
ALTER TABLE `disciplines`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT для таблицы `formofcontrol`
--
ALTER TABLE `formofcontrol`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT для таблицы `groups`
--
ALTER TABLE `groups`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=178;
--
-- AUTO_INCREMENT для таблицы `users`
--
ALTER TABLE `users`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
